package foobar;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URI;

import org.apache.logging.log4j.*;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@SpringBootTest( classes = { RestApplication.class} , webEnvironment = WebEnvironment.DEFINED_PORT)
public class HandlerTest {

    private static final Logger L = LogManager.getLogger(HandlerTest.class);

    @Autowired
    Handler handler;

    @Test
    void login() throws Exception {
        WebSocketClient ws = new WebSocketClient(new URI("ws://localhost:8080/ws/test")) {

            @Override
            public void onOpen(ServerHandshake handshakedata) {
            }

            @Override
            public void onMessage(String message) {
                L.info("{}", message);

            }

            @Override
            public void onError(Exception ex) {
            }

            @Override
            public void onClose(int code, String reason, boolean remote) {
            }
        };
        ws.connectBlocking();

        Thread.sleep(500);

        assertEquals(handler.error, 0);
    }
}
