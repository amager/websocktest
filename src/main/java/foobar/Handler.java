package foobar;


import java.util.concurrent.*;

import org.apache.logging.log4j.*;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.socket.*;

import reactor.core.publisher.Mono;

@Component
public class Handler implements WebSocketHandler {

    private static final Logger L = LogManager.getLogger(Handler.class);

    private final static int HEART_BEAT_TIME = 50;

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    int error = 0;

    @Override
    public Mono<Void> handle(WebSocketSession session) {
        scheduler.schedule(() -> sendPing(session), HEART_BEAT_TIME, TimeUnit.MILLISECONDS);

        return session.send(Mono.just(session.textMessage("Hello World")));
    }


    private void sendPing(WebSocketSession session) {
        L.debug("sent ping");
        try {
            session
                .send(Mono.just(session.pingMessage(DataBufferFactory::allocateBuffer)))
                .subscribe();
        } catch (Throwable t) {
            L.catching(t);
            error ++;
        }
    }
}